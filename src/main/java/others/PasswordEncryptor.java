package others;
import java.util.Base64;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
public class PasswordEncryptor {
    WebDriver driver;
    static String decodepassword="aGFwcHltYW4xNjM=";
    public static String getDecodedpassword() {
        return new String(Base64.getDecoder().decode(decodepassword.getBytes()));
    }

    @Test
    public void test1() {
        System.out.println(getDecodedpassword());
        driver=new FirefoxDriver();
        driver.get("https://accounts.google.com");
        driver.findElement(By.id("Email")).sendKeys("testerTest@gmail.com");
        driver.findElement(By.id("Passwd")).sendKeys(getDecodedpassword());
        driver.findElement(By.id("signIn")).click();

    }

}