package others;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageFactoryAndPOM {
    private WebDriver driver;
    @FindBy(id="locatorID")
    @CacheLookup // When it is used in the second, it will not be found again, will save time.
    private WebElement element; // element will be found by id.

    public PageFactoryAndPOM(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this); // Always be in class constructor
    }

    public void testMethod(){

    }
}
