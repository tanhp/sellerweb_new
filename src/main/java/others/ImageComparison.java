package others;

import constant.MyConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

public class ImageComparison {

    private WebDriver driver;
    public Boolean isDifferentImage(WebElement element, String imageName){
        try {
            BufferedImage expectedImage = ImageIO.read(new File(MyConstants.IMAGES_PATH + imageName));
            Screenshot logoImageScreenshot = new AShot().takeScreenshot(driver, element);
            BufferedImage actualImage = logoImageScreenshot.getImage();

            ImageDiffer imgDiff = new ImageDiffer();
            ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);
            return diff.hasDiff();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}
