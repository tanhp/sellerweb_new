package helpers;

import constant.MyConstants;
import operation.UIOperation;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import logger.MyLogger;
import utils.XLSWorker;

import static org.testng.Assert.assertTrue;
import static operation.ParallelTest.exception;

public class MyVerifys {
    WebDriver driver;
    MyHelper helper;

    public MyVerifys(WebDriver driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    public void verifyValidation(By selector, String errorMessage){
        WebElement validationMessage = helper.myBasics().getElementMayNull(selector);
        if(errorMessage.equals("")){
            if(validationMessage != null)
                assertTrue(false);
        }else{
            if(validationMessage == null)
                assertTrue(false);
            else{
                helper.myWaits().waitForElementToDisplay(selector);
                if(!validationMessage.getText().equals(errorMessage))
                    assertTrue(false);
            }
        }
    }

    public void verifyToastMessage(String message){
        WebElement toastMessage = helper.myBasics().getElementMayNull(By.xpath("//*[@class='toast toast-error' or @class='toast toast-success' or @class='toast toast-warning']"));
        if(message.equals("")) {
            if (toastMessage != null){
            	helper.myTools().logFail("[FAILURE]: Find toast message displayed in case no message set from excel.");
            }
                
        }else if(message.equals("Popup")) {
        	WebElement popup = helper.myBasics().getElement(By.xpath("//div[@class='modal-dialog']"));
            if(popup == null)
                assertTrue(false);
        }else{
        	int time = 0;
        	while(toastMessage==null) {
    			System.out.println("Find toast message...");
        		toastMessage = helper.myBasics().getElementMayNull(By.xpath("//*[@class='toast toast-error' or @class='toast toast-success' or @class='toast toast-warning']"));
        		time++;
        		if(time==5) {
                	//helper.myTools().logFail("[FAILURE]: Cannot find toast message displayed in case of a message set from excel.");
        			//return;
        			break;
        		}
        	}
            helper.myWaits().waitForElementToDisplay(By.className("toast-message"));
            MyLogger.info("Actual toastmessage: " + toastMessage.getText());
            if(!toastMessage.getText().contains(message)) {
                helper.myTools().logFail("[FAILURE]: Toast message is not matched with expected message set from excel.");
            }
        }
    }

    public void verifyUrl(String url){
        Boolean flag = true;
        helper.myWaits().waitForPageLoad();
        String currentUrl = driver.getCurrentUrl();
        System.out.println("Current URL: " + currentUrl);
        if(!currentUrl.contains(url))
            helper.myTools().logFail("Current url is not matched with expected url set from excel.");
    }

    public void validation(String fileName, String expectedResponse){
        expectedResponse = "{ " + expectedResponse + " }";
        expectedResponse = expectedResponse.replace("https://ban.sendo.vn/","baseUrl");
        System.out.println(expectedResponse);
        JSONObject expectedJsonObject = new JSONObject(expectedResponse);

        for (String key : expectedJsonObject.keySet()) {
            String expected = expectedJsonObject.get(key).toString();
            XSSFSheet sheet = XLSWorker.getSheet(XLSWorker.getWorkbook(MyConstants.INPUT_PATH + "TestValidation.xlsx"),"Message");
            String response = XLSWorker.getMessageTextByCode(sheet,expected);
            response = response.replace("baseUrl","https://ban.sendo.vn/");
            switch (key){
                case "url":
                    verifyUrl(response);
                    System.out.println("URL OK!");
                    break;
                case "toastmessage":
                    verifyToastMessage(response);
                    System.out.println("ToastMessage OK!");
                    break;
                default:
                    By selector = UIOperation.getObject(fileName, key);
                    verifyValidation(selector,response);
                    System.out.println("Validation OK!");
            }

        }
    }
}
