package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Set;

public class MySwitchs {
    WebDriver driver;
    MyHelper helper;
    String originalHandle;

    public MySwitchs(WebDriver driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    public void switchToNewWindow(){
        originalHandle = driver.getWindowHandle();
        Set handles = driver.getWindowHandles();
        for (String handle1 : driver.getWindowHandles()) {
            driver.switchTo().window(handle1);
        }
        helper.myWaits().waitForPageLoad();
    }

    public void switchToOldWindow(){
        for(String handle : driver.getWindowHandles()) {
            if (!handle.equals(originalHandle)) {
                driver.switchTo().window(handle);
                driver.close();
            }
        }
        driver.switchTo().window(originalHandle);
    }

    public void switchToNewFrame(String locator){
        driver.switchTo().frame(locator);
    }
    
    public void switchToNewFrame(WebElement element) {
    	driver.switchTo().frame(element);
    }
    

    public void switchToOldFrame(){
        driver.switchTo().defaultContent();
    }
    
    public WebElement switchToActiveElement(){
        return driver.switchTo().activeElement();
    }
}
