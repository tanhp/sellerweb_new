package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.awt.*;
import java.awt.event.KeyEvent;

public class MyKeyboards {
    WebDriver driver;
    MyHelper helper;

    public MyKeyboards(WebDriver driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    public void pressKey(By selector, String key){
        switch (key){
            case "ENTER":
                helper.myBasics().getElement(selector).sendKeys(Keys.ENTER);
                break;
            case "ESC":
                helper.myBasics().getElement(selector).sendKeys(Keys.ESCAPE);
                break;
        }
    }

    public void pressKeyByRobot(String key){
        try{
            Robot r=new Robot();
            switch (key){
                case "ENTER":
                    r.keyPress(KeyEvent.VK_ENTER);
                    r.keyRelease(KeyEvent.VK_ENTER);
                    break;
                case "ESC":
                    r.keyPress(KeyEvent.VK_ESCAPE);
                    r.keyRelease(KeyEvent.VK_ESCAPE);
                    break;
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }


    }

}
