package helpers;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestException;

public class MyWaits{
    WebDriver driver;
    MyHelper helper;
    private WebDriverWait wait;
    private int timeout = 10;

    public MyWaits(WebDriver driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    public void waitForElementToDisplay(By Selector) {
        int count = 0;
        WebElement element = helper.myBasics().getElement(Selector);
        while (!element.isDisplayed() && count < 20) {
            System.out.println("Waiting for element to display: " + Selector);
            count++;
            helper.myBasics().sleep(200);
        }
    }

    public void waitForElementTextToBeEmpty(WebElement element) {
        String text;
        try {
            text = element.getText();
            int maxRetries = 10;
            int retry = 0;
            while ((text.length() >= 1) || (retry < maxRetries)) {
                retry++;
                text = element.getText();
            }
        } catch (Exception e) {
            System.out.print(String.format("The following element could not be cleared: [%s]", element.getText()));
        }

    }

    public void waitForElementToBeVisible(By selector) {
        try {
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfElementLocated(selector));
        } catch (Exception e) {
            throw new NoSuchElementException(String.format("The following element was not visible: %s", selector));
        }
    }

    public void waitForElementToBeVisibleMayNull(By selector){
        try {
            wait = new WebDriverWait(driver, 1);
            wait.until(ExpectedConditions.presenceOfElementLocated(selector));
        } catch (Exception e) {
        }
    }

    public void waitForElementToBeInvisible(By selector){
        try {
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(selector));
        } catch (Exception e) {
            throw new NoSuchElementException(String.format("The following element was not visible: %s", selector));
        }
    }

    public void waitUntilElementIsDisplayedOnScreen(By selector) {
        try {
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
        } catch (Exception e) {
            throw new NoSuchElementException(String.format("The following element was not visible: %s ", selector));
        }
    }

    public void waitForElementToBeClickable(By selector) {
        try {
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.elementToBeClickable(selector));
            waitForAjaxDisappear();
        } catch (Exception e) {
            throw new TestException("The following element is not clickable: " + selector);
        }
    }
    
    public void waitForAjaxDisappear() {
    	WebElement ajaxLoading;
    	int time = 0;
    	do {
        	ajaxLoading = helper.myBasics().getElementMayNull(By.id("loading"));
        	helper.myBasics().sleep(1000);
        	time++;
        	System.out.println("Ajax loading detected: " + time);
        	if(time==10)
        		break;
    	}while(ajaxLoading != null && ajaxLoading.isDisplayed());
    }

    public void waitForPageLoad() {
        String state = null;
        String oldstate = null;
        try {
            int i = 0;
            while (i < 5) {
                Thread.sleep(1000);
                state = ((JavascriptExecutor)driver).executeScript("return document.readyState;").toString();
                //System.out.print("." + Character.toUpperCase(state.charAt(0)) + ".");
                if (state.equals("interactive") || state.equals("loading"))
                    break;
	            /*
	             * If browser in 'complete' state since last X seconds. Return.
	             */

                if (i == 1 && state.equals("complete")) {
                    //System.out.println();
                    return;
                }
                i++;
            }
            i = 0;
            oldstate = null;
            Thread.sleep(2000);

	        /*
	         * Now wait for state to become complete
	         */
            while (true) {
                state = ((JavascriptExecutor)driver).executeScript("return document.readyState;").toString();
                //System.out.print("." + state.charAt(0) + ".");
                if (state.equals("complete"))
                    break;

                if (state.equals(oldstate))
                    i++;
                else
                    i = 0;
	            /*
	             * If browser state is same (loading/interactive) since last 60
	             * secs. Refresh the page.
	             */
                if (i == 15 && state.equals("loading")) {
                    System.out.println("\nBrowser in " + state + " state since last 60 secs. So refreshing browser.");
                    driver.navigate().refresh();
                    System.out.print("Waiting for browser loading to complete");
                    i = 0;
                } else if (i == 6 && state.equals("interactive")) {
                    System.out.println(
                            "\nBrowser in " + state + " state since last 30 secs. So starting with execution.");
                    return;
                }

                Thread.sleep(4000);
                oldstate = state;

            }
            System.out.println();

        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
