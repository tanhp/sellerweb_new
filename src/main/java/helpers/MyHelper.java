package helpers;

import classes.*;
import org.openqa.selenium.*;

import classes.MyClasses;
import pages.*;

public class MyHelper {
    private WebDriver driver;

    public MyHelper(WebDriver driver){
        this.driver = driver;
    }

    private MyJavascripts myJavascripts;
    private MyNavigates myNavigates;
    private MyWaits myWaits;
    private MyVerifys myVerifys;
    private MyBasics myBasics;
    private MyActions myActions;
    private MyTools myTools;
    private MySwitchs mySwitchs;
    private MyClasses myObjects;
    private MyKeyboards myKeyboards;

    public MyJavascripts myJavascripts(){
        if(this.myJavascripts == null)
            myJavascripts = new MyJavascripts(driver,this);
        return myJavascripts;
    }

    public MyNavigates myNavigagtes(){
        if(myNavigates == null)
            myNavigates = new MyNavigates(driver,this);
        return myNavigates;
    }

    public MyWaits myWaits(){
        if(myWaits == null)
            myWaits = new MyWaits(driver,this);
        return myWaits;
    }

    public MyVerifys myVerifys(){
        if(myVerifys == null)
            myVerifys = new MyVerifys(driver, this);
        return myVerifys;
    }

    public MyBasics myBasics(){
        if(myBasics == null)
            myBasics = new MyBasics(driver, this);
        return myBasics;
    }

    public MyActions myActions(){
        if(myActions == null)
            myActions = new MyActions(driver, this);
        return myActions;
    }

    public MyTools myTools(){
        if(myTools == null)
            myTools = new MyTools(driver, this);
        return myTools;
    }

    public MySwitchs mySwitchs(){
        if(mySwitchs == null)
            mySwitchs = new MySwitchs(driver, this);
        return mySwitchs;
    }

    public MyClasses myObjects(){
        if(myObjects == null)
            myObjects = new MyClasses();
        return myObjects;
    }


    public MyKeyboards myKeyboards(){
        if(myKeyboards == null)
            myKeyboards = new MyKeyboards(driver,this);
        return myKeyboards;
    }


}



