package helpers;

import constant.MyConstants;
import org.openqa.selenium.WebDriver;
import org.testng.TestException;

public class MyNavigates{
    WebDriver driver;
    MyHelper helper;

    public MyNavigates(WebDriver driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    public void navigateToURL(String URL) {
        if(!MyConstants.BASE_SELLER_URL.equals("https://ban.sendo.vn"))
            URL = URL.replace("https://ban.sendo.vn",MyConstants.BASE_SELLER_URL);
        try {
            driver.navigate().to(URL);
            helper.myVerifys().verifyToastMessage("");
            helper.myWaits().waitForPageLoad();
        } catch (Exception e) {
            System.out.println("FAILURE: URL did not load: " + URL);
            throw new TestException("URL did not load");
        }
    }

    public void navigateBack() {
        try {
            driver.navigate().back();
        } catch (Exception e) {
            System.out.println("FAILURE: Could not navigate back to previous page.");
            throw new TestException("Could not navigate back to previous page.");
        }
    }

    public void reloadPage(){
        driver.navigate().refresh();
        helper.myWaits().waitForPageLoad();
    }

}
