package helpers;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.testng.TestException;

import logger.MyLogger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static operation.ParallelTest.exception;
import static org.testng.Assert.assertTrue;

public class MyBasics {
    WebDriver driver;
    MyHelper helper;

    public MyBasics(WebDriver driver, MyHelper helper){
        this.driver = driver;
        this.helper = helper;
    }

    public String getPageTitle() {
        try {
            return driver.getTitle();
        } catch (Exception e) {
            throw new TestException(String.format("Current page title is: %s", driver.getTitle()));
        }
    }

    public String getCurrentURL() {
        try {
            return driver.getCurrentUrl();
        } catch (Exception e) {
            throw new TestException(String.format("Current URL is: %s", driver.getCurrentUrl()));
        }
    }

    public WebElement getElement(By selector) {
        try {
            helper.myWaits().waitForElementToBeVisible(selector);
            return driver.findElement(selector);
        } catch (Exception e) {
            exception = String.format("Element %s does not exist - proceeding", selector);
            MyLogger.error(exception);
        }
        return null;
    }

    public WebElement getElementMayNull(By selector){
        try {
            helper.myWaits().waitForElementToBeVisibleMayNull(selector);
            return driver.findElement(selector);
        } catch (Exception e) {
        }
        return null;
    }

    public String getElementText(By selector) {
        helper.myWaits().waitUntilElementIsDisplayedOnScreen(selector);
        try {
            return StringUtils.trim(driver.findElement(selector).getText());
        } catch (Exception e) {
            exception = String.format("Element %s does not exist - proceeding", selector);
            MyLogger.error(exception);
        }
        return null;
    }

    public String getElementCssValue(By selector, String property){
        try{
            return driver.findElement(selector).getCssValue(property);
        } catch (Exception e){
            exception = String.format("Element %s does not exist - proceeding", selector);
            MyLogger.error(exception);
        }
        return null;
    }

    public String getElementAttribute(By selector, String property){
        try{
            return driver.findElement(selector).getAttribute(property);
        } catch (Exception e){
            exception = String.format("Element %s does not exist - proceeding", selector);
            MyLogger.error(exception);
        }
        return null;
    }

    public List<WebElement> getElements(By Selector) {
        helper.myWaits().waitForElementToBeVisible(Selector);
        try {
            return driver.findElements(Selector);
        } catch (Exception e) {
            exception = String.format("The following element did not display: [%s] ", Selector.toString());
            throw new NoSuchElementException(exception);
        }
    }

    public List<String> getListOfElementTexts(By selector) {
        List<String> elementList = new ArrayList<String>();
        List<WebElement> elements = getElements(selector);

        for (WebElement element : elements) {
            if (element == null) {
                throw new TestException("Some elements in the list do not exist");
            }
            if (element.isDisplayed()) {
                elementList.add(element.getText().trim());
            }
        }
        return elementList;
    }

    public WebElement getElementNotVisible(By selector){
        // Solution for element is not visible
        int elementSize = helper.myBasics().getElements(selector).size();
        return helper.myBasics().getElements(selector).get(elementSize-1);
    }

    public void clickStaleElement(By selector){
        // Solution for stale element
        for(int i=0; i<3; i++){
            try{
                helper.myBasics().getElement(selector).click();
                break;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public void click(By selector) {
        WebElement element = getElement(selector);
        helper.myWaits().waitForAjaxDisappear();
        helper.myWaits().waitForElementToDisplay(selector);
        helper.myWaits().waitForElementToBeClickable(selector);
        int count = 0;
        while(count < 2){
            try{
                element.click();
                break;
            }catch (StaleElementReferenceException e){
                element = getElement(selector);
                System.out.println("Stale element detected: " + count);
                count++;
            }catch (Exception e){
                exception = String.format("The following element is not clickable: [%s]", selector);
                MyLogger.error(e.getMessage());
                throw new TestException(String.format("The following element is not clickable: [%s]", selector));
            }

        }
    }


    public void clickMayNull(By selector){
    	try {
	        WebElement element;
	        int time = 0;
	        do{
	            element = getElementMayNull(selector);
	            if(element != null){
	                helper.myBasics().click(selector);
	                break;
	            }
	            System.out.println(time);
	            time++;
	            if(time==10)
	                break;
	        }while(element==null);
    	}catch(Exception e) {
    		
    	}
    }

    public void sendKeys(By selector, String value) {
        WebElement element = getElement(selector);
        helper.myWaits().waitForAjaxDisappear();
        helper.myWaits().waitForElementToDisplay(selector);
        clearField(element);
        try {
            element.sendKeys(value);
        } catch (Exception e) {
            exception = String.format("Error in sending [%s] to the following element: [%s]", value, selector.toString());
            MyLogger.error(exception);
            throw new TestException(exception);
        }
    }

    public void clearField(WebElement element) {
        try {
            element.clear();
            helper.myWaits().waitForElementTextToBeEmpty(element);
        } catch (Exception e) {
            exception = String.format("The following element could not be cleared: [%s]", element.getText());
            System.out.print(String.format("The following element could not be cleared: [%s]", element.getText()));
        }
    }

    public void sleep(final long millis) {
        //System.out.println((String.format("sleeping %d ms", millis)));
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void selectIfOptionTextContains(By selector, String searchCriteria) {
        helper.myWaits().waitForElementToBeClickable(selector);
        Select dropdown = new Select(getElement(selector));

        List<WebElement> options = dropdown.getOptions();

        String optionText = "";

        if (options == null) {
            throw new TestException("Options for the dropdown list cannot be found.");
        }

        for (WebElement option : options) {

            optionText = option.getText().trim();
            boolean isOptionDisplayed = option.isDisplayed();

            if (optionText.contains(searchCriteria) && isOptionDisplayed) {
                try {
                    dropdown.selectByVisibleText(optionText);
                    break;
                } catch (Exception e) {
                    throw new NoSuchElementException(String.format("The following element did not display: [%s] ", selector.toString()));
                }
            }
        }
    }

    public void selectIfOptionTextEquals(By selector, String searchCriteria) {

        if(searchCriteria.equals(""))
            return;

        helper.myWaits().waitForElementToBeClickable(selector);
        Select dropdown = new Select(getElement(selector));

        List<WebElement> options = dropdown.getOptions();

        String optionText = "";

        if (options == null) {
            throw new TestException("Options for the dropdown list cannot be found.");
        }

        for (WebElement option : options) {

            optionText = option.getText().trim();
            boolean isOptionDisplayed = option.isDisplayed();

            if (optionText.equals(searchCriteria) && isOptionDisplayed) {
                try {
                    dropdown.selectByVisibleText(optionText);
                    System.out.println(optionText + " is selected");
                    break;
                } catch (Exception e) {
                    throw new NoSuchElementException(String.format("The following element did not display: [%s] ", selector.toString()));
                }
            }
        }
    }

    public List<String> getDropdownValues(By selector) {
        helper.myWaits().waitForElementToDisplay(selector);
        Select dropdown = new Select(getElement(selector));
        List<String> elementList = new ArrayList<String>();

        List<WebElement> allValues = dropdown.getOptions();

        if (allValues == null) {
            throw new TestException("Some elements in the list do not exist");
        }

        for (WebElement value : allValues) {
            if (value.isDisplayed()) {
                elementList.add(value.getText().trim());
            }
        }
        return elementList;
    }

    public String getTextFromInput(By selector){
        WebElement element = getElement(selector);
        System.out.println(element.getAttribute("value"));
        return element.getAttribute("value");
    }

    public String getValueFromCheckbox(By selector){
        WebElement element = getElement(selector);
        if(element.isSelected())
            return "YES";
        else
            return "NO";
    }

    public String getValueFromMultiDropDown(By selector){
        WebElement input = getElement(selector);
        List<WebElement> values = input.findElements(By.className("item"));
        System.out.println("Sizes list " + values.size());
        String result = "";
        for(int i=0; i<values.size(); i++){
            if(i==0)
                result = result + values.get(i).getText().replace("\n×","");
            else
                result = result + ", " + values.get(i).getText().replace("\n×","");
        }
        System.out.println(result);
        return result;
    }

    public void setElementWait(int millisecond) {
        // Wait for element
        driver.manage().timeouts().implicitlyWait(millisecond, TimeUnit.MILLISECONDS);
    }

    public void selectFromDropDown(By selector, String value){
        Select dropdown = new Select(getElement(selector));
        dropdown.selectByVisibleText(value);
    }

    public void selectOneFromDropDown(By selector, String option){
        if(option.equals(""))
            return;
        click(selector);
        click(By.xpath("//div[contains(@class,'option') and text()='" + option + "']"));
        //clickByJS(driver.findElement(By.xpath("//div[contains(@class,'option) and text()='" + option + "']")));
    }

    public void setCheckbox(By selector, String value){
        WebElement checkBox = getElement(selector);
        if(value.equals("YES")){
            if(!checkBox.isSelected())
                helper.myJavascripts().clickByJS(checkBox);
        }else{
            if(checkBox.isSelected())
                helper.myJavascripts().clickByJS(checkBox);
        }
    }

    public void setClick(By selector){
        sleep(2000);
        helper.myJavascripts().clickByJS(driver.findElement(selector));
        helper.myVerifys().verifyToastMessage("");
    }

    public void setClick2(By selector){
        sleep(2000);
        helper.myJavascripts().clickByJS(driver.findElement(selector));
    }

    public void selectMultiFromDropDown(By selector, String options){
        if(options.equals(""))
            return;
        helper.myJavascripts().clickByJS(driver.findElement(selector));
        String[] optionList = options.split(", ");
        for(int i=1; i<=optionList.length; i++){
            click(By.xpath("//div[contains(@class,'option') and text()='" + optionList[i-1] + "']"));
        }
        driver.findElement(selector).sendKeys(Keys.ESCAPE);
    }

    public String getIdFromUrl(){
        String url = driver.getCurrentUrl();
        String[] sections = url.split("/");
        String id = sections[sections.length-1];
        return id;
    }

    public void waitAjaxDisappear(By selector){
        try {
            helper.myWaits().waitForElementToBeInvisible(selector);
        } catch (Exception e) {
            exception = String.format("Element %s does not exist - proceeding", selector);
            System.out.println(exception);
        }
    }

    public boolean isAlertPresent(){
        try {
            driver.switchTo().alert();
            driver.switchTo().alert().accept();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }
    
    public void verifySecurity(String value){
    	helper.myBasics().sleep(1500);
        helper.myBasics().clickMayNull(By.xpath("//button[contains(@class,'CloseButton')]"));
    	By selector = By.name(value);
    	WebElement element = helper.myBasics().getElement(selector);
    	switch(value) {
    		case "click":
    			if(element!=null) {
    				helper.myJavascripts().scrollDownPage("500");
    				helper.myActions().mouseHover(element);
        			helper.myBasics().click(selector);
    			}
    			break;
    		case "mouseover":
    		case "mousemove":
    			if(element!=null) {
    				helper.myJavascripts().scrollToElement(element);
    				helper.myActions().mouseHover(element);
    			}
    			break;
    		case "popup":
    			WebElement popup = helper.myBasics().getElement(By.name("btnOK"));
    			if(popup==null)
    				assertTrue(false);
    	}
        if(helper.myBasics().isAlertPresent()) {
        	helper.myTools().logFail("XSS detected!");
        }
        System.out.println("Verify Security!");
    }

}
