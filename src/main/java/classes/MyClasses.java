package classes;

import utils.FormatUtils;

public class MyClasses {

    private CProduct product;
    private CShopInfo shopInfo;
    private CPromotion promotion;
    private CChat chat;
    private CShippingSupport shipping;
    private CPrivateOffer privateOffer;
    private CSalesOrder salesOrder;
    private CCarrier carrier;
    private CBlog blog;
    private CBanner banner;
    private CVoucher voucher;

    public MyClasses(){
        product = new CProduct();
        shopInfo = new CShopInfo();
        promotion = new CPromotion();
        chat = new CChat();
        shipping = new CShippingSupport();
        privateOffer = new CPrivateOffer();
        salesOrder = new CSalesOrder();
        carrier = new CCarrier();
        blog = new CBlog();
        banner = new CBanner();
        voucher = new CVoucher();
    }

    public void setProductObject(String object, String value){
        switch (object) {
            case "pg_ProductDetail_Category":
                product.setCatePath(value);
                break;
            case "pg_ProductDetail_CategoryPath":
                product.setCatePath2(value);
                break;
            case "pg_ProductDetail_ProductName":
            case "pg_QuickEdit_ProductName":
                product.setProductName(value);
                break;
            case "pg_ProductDetail_ProductSKU":
            case "pg_QuickEdit_ProductSku":
                product.setProductSku(value);
                break;
            case "pg_ProductDetail_ProductPrice":
            case "pg_QuickEdit_ProductPrice":
                product.setProductPrice(value);
                break;
            case "pg_ProductDetail_InStock":
            case "pg_QuickEdit_ProductInstock":
                product.setInStock(value);
                break;
            case "pg_ProductDetail_Size":
                product.setProductSize(value);
                break;
            case "pg_ProductDetail_Color":
                product.setProductColor(value);
                break;
            case "pg_ProductList_InStock":
                product.setInStock(value);
                break;
            case "pg_ProductDetail_PromotionPrice":
            case "pg_QuickEdit_PromotionPrice":
                product.setDiscountMoney(value);
                break;
            case "pg_ProductDetail_StockQuality":
            case "pg_QuickEdit_StockQuality":
                product.setStockQuantity(value);
                break;

        }
    }

    public void setProductFromId(String value, String id){
        switch (value) {
            case "PRODUCT":
                product.setProductId(id);
                product.setBuyerUrl(id);
                break;
            case "PROMOTION":
                promotion.setPromotionId(id);
                break;
            case "BLOG":
                blog.setBlogId(id);
        }
    }

    public CProduct getProductObject(){
        return product;
    }

    public void setShopInfoObject(String object, String value){
        switch (object){
            case "pg_ShopInfo_ShopName":
                shopInfo.setShopName(value);
                break;
            case "pg_ShopInfo_ShopUrl":
                shopInfo.setShopUrl(value);
                break;
            case "pg_ShopInfo_Slogan":
                shopInfo.setSlogan(value);
                break;
            case "pg_ShopInfo_ShortDescription":
                shopInfo.setShortDescription(value);
                break;
            case "pg_ShopInfo_SellerName":
                shopInfo.setSellerName(value);
                break;
            case "pg_ShopInfo_Phone":
                shopInfo.setPhone(value);
                break;
            case "pg_ShopInfo_Email":
                shopInfo.setEmail(value);
                break;
            case "pg_ShopInfo_TaxCode":
                shopInfo.setTaxCode(value);
                break;
            case "pg_ShopInfo_ShopAddress":
                shopInfo.setShopAddress(value);
                break;
            case "pg_ShopInfo_ShopCity":
                shopInfo.setShopCity(value);
                break;
            case "pg_ShopInfo_ShopDistrict":
                shopInfo.setShopDistrict(value);
                break;
            case "pg_ShopInfo_ShopWard":
                shopInfo.setShopWard(value);
                break;
        }
    }

    public CShopInfo getShopInfoObject(){
        return shopInfo;
    }

    public void setPromotionObject(String object, String value){
        switch (object){
            case "pg_Promotion_PromotionName":
                promotion.setPromotionName(value);
                break;
            case "pg_Promotion_DiscountAmount":
                if(Integer.valueOf(value) < 1000){
                    promotion.setDiscountPercent(value);
                    product.setDiscountPercent(value);
                }else{
                    promotion.setDiscountMoney(value);
                    product.setDiscountMoney(value);
                }


                break;
            case "pg_Promotion_PromotionDate":
                promotion.setPromotionDateTmp(value);
                break;
            case "pg_Promotion_PromotionDate1":
                promotion.setPromotionDate(value);
                break;
        }
    }

    public CPromotion getPromotionObject(){
        return promotion;
    }

    public void setChatObject(String object, String value){
        switch (object){
            case "pg_Chat_ChatIFrame":
                chat.setUserName(value);
                break;
            case "pg_ChatDetail_ChatTextBox":
                chat.setContentMessage(value);
                chat.setShortMessage(value);
                break;
        }
    }

    public CChat getChatObject(){
        return chat;
    }

    public void setShippingSupportObject(String object, String value){
        switch (object){
            case "pg_ShippingSupport_OrderAmount01":
                shipping.setOrderAmount01(value);
                break;
            case "pg_ShippingSupport_SellerSupportFee01":
                shipping.setSupportFee01(value);
                break;
            case "pg_ShippingSupport_Effect01":
                shipping.setEffect01(value);
                break;
            case "pg_ShippingSupport_OrderAmount02":
                shipping.setOrderAmount02(value);
                break;
            case "pg_ShippingSupport_SellerSupportFee02":
                shipping.setSupportFee02(value);
                break;
            case "pg_ShippingSupport_Effect02":
                shipping.setEffect02(value);
                break;
        }
    }
    public CShippingSupport getShippingSupport(){
        return shipping;
    }

    public void setPrivateOfferObject(String object, String value){
        privateOffer.setProductName(product.getProductName());
        privateOffer.setOriginalMoney(product.getProductPrice());
        switch (object){
            case "pg_PrivateOfferDetail_DiscountAmountPercent":
                privateOffer.setDiscountPercent(value);
                break;
            case "pg_PrivateOfferDetail_DiscountAmountMoney":
                privateOffer.setDiscountMoney(value);
                break;
            case "pg_PrivateOfferDetail_ExpireDate":
                privateOffer.setExpiredDate(value);
                break;
        }
    }

    public CPrivateOffer getPrivateOfferObject(){
        return privateOffer;
    }

    public void setSalesOrderObject(String object, String value){
        switch(object){
            case "pg_Buyer_OrderNumber":
                salesOrder.setOrderNumber(value);
                break;
        }
    }

    public CSalesOrder getSalesOrderObject(){
        return salesOrder;
    }

    public CCarrier getCarrierObject(){
        return carrier;
    }

    public void setCarrierObject(String keyword, String carrierName, String isChecked){
        if(!keyword.equals("VERIFY_NOSENMALLCARRIER") && carrierName.contains("ecom_shipping") && isChecked.equals("YES")){
            carrier.setCurrentCarrier(carrierName);
            carrier.setUrlAndCity();
        }

    }

    public CBlog getBlogObject(){
        return blog;
    }

    public void setBlogObject(String object, String value){
        switch(object){
            case "pg_BlogDetail_BlogName":
                blog.setName(value);
                break;
            case "pg_BlogDetail_BlogDescription":
                blog.setDescription(value);
                break;
            case "pg_BlogDetail_BlogTag":
                blog.setTag(value);
                break;
            case "pg_BlogDetail_BlogContent":
                blog.setContent(value);
                break;
        }
    }

    public CBanner getBannerObject(){
        return banner;
    }

    public void setBannerObject(String object, String value){
        switch (object){
            case "pg_BannerDetail_SystemURL":
                banner.setSystemURL(value);
                break;
        }
    }

    public CVoucher getVoucherObject(){
        return voucher;
    }

    public void setVoucherObject(String object, String value){
        switch (object){
            case "pg_Voucher_VoucherCode":
                voucher.setVoucherCode(value);
                break;
            case "pg_Voucher_IsDiscountPercent":
                if(value.equals("YES"))
                    voucher.setDiscountPercent(true);
                else
                    voucher.setDiscountPercent(false);
                break;
            case "pg_Voucher_IsDiscountMoney":
                if(value.equals("YES"))
                    voucher.setDiscountMoney(true);
                else
                    voucher.setDiscountMoney(false);
                break;
            case "pg_Voucher_PercentAmount":
                if(value.equals("") || value.equals("NO"))
                    value = "0";
                voucher.setPercentAmount(Integer.valueOf(value));
                break;
            case "pg_Voucher_MaxVoucherAmount":
                if(value.equals("") || value.equals("NO"))
                    value = "0";
                voucher.setMaxVoucherAmount(Long.valueOf(value));
                break;
            case "pg_Voucher_DiscountAmount":
                if(value.equals("") || value.equals("NO"))
                    value = "0";
                voucher.setDiscountAmount(Long.valueOf(value));
                break;
            case "pg_Voucher_MinOrderAmount":
                if(value.equals(""))
                    value = "0";
                voucher.setMinOrderAmount(Long.valueOf(value));
                break;
            case "pg_Voucher_NumOfUse":
                if(value.equals(""))
                    value = "0";
                voucher.setNumOfUse(Integer.valueOf(value));
                break;
            case "pg_Voucher_StartDate":
                if(value.equals("TOMORROW"))
                    value = FormatUtils.getCurrentDate();
                voucher.setApprovedDate(value);
                break;
            case "pg_Voucher_EndDate":
                if(value.equals("TOMORROW"))
                    value = FormatUtils.getCurrentDate();
                voucher.setExpireDate(value);
                break;
            case "pg_Voucher_Notes":
                voucher.setNotes(value);
                break;
            case "IsAllowShowOnBuyerPage":
                if(value.equals("YES"))
                    voucher.setAllowShowOnBuyerPage(true);
                else
                    voucher.setAllowShowOnBuyerPage(false);
                break;
        }
    }

    public void setProductVariantObject(String object, String value){
        switch (object){
            case "pg_ProductVariant_AttributeName":
                product.setVariant_AttributeName(value);
                break;
            case "pg_ProductVariant_AttributeValue":
                product.setVariant_AttributeValue(value);
                break;
            case "pg_ProductVariant_Price":
            case "pg_QuickEditVariant_Price":
                product.setVariant_Price(value);
                break;
            case "pg_ProductVariant_SpecialPrice":
            case "pg_QuickEditVariant_SpecialPrice":
                product.setVariant_SpecialPrice(value);
                break;
            case "pg_ProductVariant_PromotionDate":
            case "pg_QuickEditVariant_PromotionDate":
                product.setVariant_PromotionDate(value);
                break;
            case "pg_ProductVariant_StockQuantity":
            case "pg_QuickEditVariant_StockQuantity":
            	product.setVariant_StockQuantity(value);
            	break;
            case "pg_ProductVariant_SKU":
            case "pg_QuickEditVariant_SKU":
                product.setVariant_SKU(value);
                break;
        }
    }
}
