package classes;

import utils.FormatUtils;

public class CPromotion {
    private String promotionName;
    private String promotionDate;
    private String promotionDateTmp;
    private String discountPercent;
    private String discountMoney;
    private String discountPrice;
    private String promotionId;

    public String getPromotionDateTmp() {
        return promotionDateTmp;
    }

    public void setPromotionDateTmp(String promotionDateTmp) {
        this.promotionDateTmp = promotionDateTmp; // Hôm nay
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }


    public String getPromotionDate() {
        return promotionDate;
    }
    public void setPromotionDate(String promotionDate) {
    	this.promotionDate = promotionDate; // dd/mm/yyyy - dd/mm/yyyy
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountAmount) {
        this.discountPercent = discountAmount;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getDiscountMoney() {
        return discountMoney;
    }

    public void setDiscountMoney(String discountMoney) {
        discountMoney = FormatUtils.formatNumber(discountMoney, "#0,000");
        this.discountMoney = discountMoney;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

}
