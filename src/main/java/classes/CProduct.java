package classes;

import org.testng.annotations.Test;

import utils.FormatUtils;

public class CProduct {
    private String productName;
    private String productSku;
    private String catePath;
    private String productColor;
    private String productSize;
    private String productPrice;
    private String discountPercent;
    private String discountMoney;
    private String discountPrice;
    private Boolean inStock;

    private String stockQuantity;
    private String productId;
    private String buyerUrl;
    
    private Boolean isConfig = false;

    private String variant_AttributeName;
    private String variant_AttributeValue;

    private String variant_Price;
    private String variant_SpecialPrice;
    private String variant_PromotionDate;
    private String variant_StockQuantity;
    private String variant_SKU;

    public Boolean getConfig() {
        return isConfig;
    }

    public void setConfig(Boolean config) {
        isConfig = config;
    }
    
    public String getVariant_AttributeName() {
        return variant_AttributeName;
    }

    public void setVariant_AttributeName(String variant_AttributeName) {
    	setConfig(true);
        this.variant_AttributeName = variant_AttributeName;
    }

    public String getVariant_AttributeValue() {
        return variant_AttributeValue;
    }

    public void setVariant_AttributeValue(String variant_AttributeValue) {
        this.variant_AttributeValue = variant_AttributeValue;
    }

    public String getVariant_Price() {
        return variant_Price;
    }

    public void setVariant_Price(String variant_Price) {
        this.variant_Price = variant_Price;
        setProductPrice(variant_Price);
    }

    public String getVariant_SpecialPrice() {
        return variant_SpecialPrice;
    }

    public void setVariant_SpecialPrice(String variant_SpecialPrice) {
        this.variant_SpecialPrice = variant_SpecialPrice;
        setDiscountMoney(variant_SpecialPrice);
    }

    public String getVariant_PromotionDate() {
        return variant_PromotionDate;
    }

    public void setVariant_PromotionDate(String variant_PromotionDate) {
        this.variant_PromotionDate = variant_PromotionDate;
    }

    public String getVariant_StockQuantity() {
        return variant_StockQuantity;
    }

    public void setVariant_StockQuantity(String variant_StockQuantity) {
        this.variant_StockQuantity = variant_StockQuantity;
    }

    public String getVariant_SKU() {
        return variant_SKU;
    }

    public void setVariant_SKU(String variant_SKU) {
        this.variant_SKU = variant_SKU;
    }

    public String getProductName(){
        return productName;
    }

    public String getProductSku(){
        return productSku;
    }

    public String getCatePath(){
        return catePath;
    }

    public String getProductColor(){
        return productColor;
    }

    public String getProductSize(){
        return productSize;
    }

    public String getProductPrice(){
        return productPrice;
    }

    public String getDiscountPrice(){
        float discountPrice;
        if(this.discountPercent != null) {
            discountPrice = Long.valueOf(this.productPrice.replace(",", ""))
                    * (1 - (Long.valueOf(this.discountPercent) / 100.0f));
        }else{
            discountPrice = Long.valueOf(this.productPrice.replace(",","")) - Long.valueOf(this.discountMoney);
        }
        return FormatUtils.formatNumber(String.valueOf(discountPrice).replace(".0", ""), "#,000");
    }

    public Boolean getInStock(){
        return inStock;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductName(String productName){
        if(!productName.equals(""))
            this.productName = productName;
    }

    public void setProductSku(String productSku){
        if(!productSku.equals(""))
            this.productSku = productSku;
    }

    public void setCatePath(String catePath){
        //catePath = FormatUtils.matchAndReplaceNonEnglishChar(catePath) + "/";
        if(!catePath.equals(""))
            this.catePath = catePath;
    }

    public void setCatePath2(String catePath2){
        this.catePath = catePath2.replace(" > ",", ");
    }

    public void setProductColor(String productColor){
        this.productColor = productColor;
    }

    public void setProductSize(String productSize){
        this.productSize = productSize;
    }

    public void setProductPrice(String productPrice){
        if(!productPrice.equals("") && !productPrice.equals("NO")) {
            productPrice = productPrice.replace(",","");
            productPrice = FormatUtils.formatNumber(productPrice, "#0,000");
            System.out.println(productPrice);
            this.productPrice = productPrice;
        }
    }
    
    @Test
    public void showPrice() {
    	setProductPrice("95000");
    }

    public void setDiscountPrice(String discountPrice){

    }

    public void setInStock(String inStock){
        if(inStock.equals("YES"))
            this.inStock = true;
        else
            this.inStock = false;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBuyerUrl() {
        return buyerUrl;
    }

    public void setBuyerUrl(String productId) {
        this.buyerUrl = "https://www.sendo.vn/" + FormatUtils.matchAndReplaceNonEnglishChar(productName) + "-" + productId + ".html";
        System.out.println(this.buyerUrl);
    }

    public void setDiscountPercent(String discountAmount){
        if(!discountAmount.equals("") && !discountAmount.equals("NO")) {
            discountAmount = discountAmount.replace(",","");
            discountAmount = FormatUtils.formatNumber(discountAmount, "#0,000");
            this.discountPercent = discountAmount;
        }
    }

    public void setDiscountMoney(String discountAmount){
        System.out.println("Discout amount: " +  discountAmount);
        if(!discountAmount.equals("") && !discountAmount.equals("NO")) {
            discountAmount = discountAmount.replace(",","");
            discountAmount = FormatUtils.formatNumber(discountAmount, "#0,000");
            this.discountMoney = discountAmount;
            System.out.println("Discout money: " +  discountAmount);
        }
    }

    public String getDiscountPercent(){
        return this.discountPercent;
    }


    public String getDiscountMoney() {return this.discountMoney; }

    public String getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(String stockQuantity) {
        this.stockQuantity = stockQuantity;
    }
}
