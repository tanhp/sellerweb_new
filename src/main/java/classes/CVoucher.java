package classes;

public class CVoucher {
    private String voucherCode;
    private String discountType;
    private Boolean isDiscountPercent;
    private Boolean isDiscountMoney;
    private int percentAmount;
    private long maxVoucherAmount;
    private long discountAmount;
    private long minOrderAmount;
    private int numOfUse;
    private String approvedDate;
    private String expireDate;
    private String notes;
    private Boolean isAllowShowOnBuyerPage;

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public int getPercentAmount() {
        return percentAmount;
    }

    public void setPercentAmount(int percentAmount) {
        this.percentAmount = percentAmount;
    }

    public long getMaxVoucherAmount() {
        return maxVoucherAmount;
    }

    public void setMaxVoucherAmount(long maxVoucherAmount) {
        this.maxVoucherAmount = maxVoucherAmount;
    }

    public long getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(long discountAmount) {
        this.discountAmount = discountAmount;
    }

    public long getMinOrderAmount() {
        return minOrderAmount;
    }

    public void setMinOrderAmount(long minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }

    public int getNumOfUse() {
        return numOfUse;
    }

    public void setNumOfUse(int numOfUse) {
        this.numOfUse = numOfUse;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Boolean getAllowShowOnBuyerPage() {
        return isAllowShowOnBuyerPage;
    }

    public void setAllowShowOnBuyerPage(Boolean allowShowOnBuyerPage) {
        isAllowShowOnBuyerPage = allowShowOnBuyerPage;
    }

    public Boolean getDiscountPercent() {
        return isDiscountPercent;
    }

    public void setDiscountPercent(Boolean discountPercent) {
        isDiscountPercent = discountPercent;
    }

    public Boolean getDiscountMoney() {
        return isDiscountMoney;
    }

    public void setDiscountMoney(Boolean discountMoney) {
        isDiscountMoney = discountMoney;
    }


}
