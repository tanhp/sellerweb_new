package classes;

public class CBlog {
    private String name;
    private String description;
    private String tag;
    private String content;
    private String blogId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBlogUrl(){
        String url = "https://www.sendo.vn/shop/shopbaby47/blog/" + this.name + "/" + this.blogId;
        return url;
    }

    public void setBlogId(String id){
        this.blogId = id;
    }
}
