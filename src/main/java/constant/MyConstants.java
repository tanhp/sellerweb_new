package constant;

public class MyConstants {

    /* PATH_CONSTANTS */

    public static String SYSTEM_PATH = System.getProperty("user.dir");

    public static String DRIVER_PATH = SYSTEM_PATH + "/drivers/";
    public static String getChromeDriverPath(){
        System.out.println(System.getProperty("os.name"));
        if(System.getProperty("os.name").startsWith("Windows"))
            return DRIVER_PATH + "chromedriver.exe";
        else if(System.getProperty("os.name").startsWith("Mac"))
            return DRIVER_PATH + "os_chromedriver";
        else
            return DRIVER_PATH + "linux_chromedriver";
    }
    public static String FIREFOX_DRIVER_PATH = DRIVER_PATH + "geckodriver.exe";
    public static String IE_DRIVER_PATH = DRIVER_PATH + "IEDriverServer.exe";

    public static String CONFIG_PATH = SYSTEM_PATH + "/configs/";
    public static String INPUT_PATH = SYSTEM_PATH + "/input/";
    public static String OUTPUT_PATH = SYSTEM_PATH + "/output/";
    public static String EXCEL_OUTPUT_PATH = OUTPUT_PATH + "excelOutput/";
    public static String IMAGES_PATH = SYSTEM_PATH + "/images/";
    public static String OBJECT_PATH = SYSTEM_PATH + "/properties/";
    public static String SCREENSHOT_PATH = SYSTEM_PATH + "/screenshots/";
    public static String HTMLREPORT_PATH = SYSTEM_PATH + "/htmlreports/";
    public static String LOG_PATH = SYSTEM_PATH + "/logs/log4j/";


    /* URL_CONSTANTS */

    public static String SERVER_URL = "http://172.30.119.110:81/smc";
    public static String SCREENSHOT_URL = SERVER_URL + "/screenshots/";
    public static String HTMLREPORT_URL = SERVER_URL + "/htmlreports/";

    /* DATABASE */
    public static String DB_URL = "jdbc:mysql://172.30.119.111";
    public static String DB_USERNAME = "autoUser";
    public static String DB_PASSWORD = "abcd@1234";
    public static String DB_DATABASENAME = "test";
    public static String DB_TABLENAME = "seller";

    /* ENVIRONMENT */
    public static String ENVIRONMENT = "Seller - Production";

    /* DOMAIN */

    public static String BASE_SELLER_URL = "https://ban.sendo.vn";
    public static String BASE_BUYER_URL = "https://sendo.vn";

    public static final String BROWSER = "chrome";

    // CONFIG
    public static int timezoneOffset = 6;

    // Buyer Account
    public static String BUYER_USERNAME="tester01gm@gmail.com";
    public static String BUYER_PASSWORD="123456";

    // Skype account
    public static String SKYPE_USERNAME="mon.mon236";
    public static String SKYPE_PASSWORD = "mon8245679311";
    public static String SKYPE_IDENTITY_TAN = "8:tanhopham1990";
    public static String SKYPE_IDENTITY_BICH = "8:bichbaby47";
    public static String SKYPE_IDENTITY_GROUP = "19:4899829c0e434b2e888c3b6eaae77797@thread.skype";


}
