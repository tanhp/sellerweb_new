package database;

import constant.MyConstants;

import java.sql.*;
import java.util.List;

public class Database {
    private Connection connect;
    private Statement statement;

    public Connection getConnection() {
        try {
            connect = DriverManager.getConnection(MyConstants.DB_URL, MyConstants.DB_USERNAME, MyConstants.DB_PASSWORD);
        } catch (SQLException ex) {
            System.out.println("Failed to connect database: " + ex.getMessage());
        }
        return connect;
    }

    public Statement useDatabase(String databaseName) {
        try {
            statement = connect.createStatement();
            statement.execute("USE " + databaseName);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return statement;
    }

    public void insertIntoDatabase(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "("
                + columns.get(0) + ","
                + columns.get(1) + ","
                + columns.get(2) + ","
                + columns.get(3) + ","
                + columns.get(4) + ","
                + columns.get(5) + ","
                + columns.get(6) + ","
                + columns.get(7) + ","
                + columns.get(8) + ","
                + columns.get(9) + ","
                + columns.get(10) + ","
                + columns.get(11) + ") "
                + "VALUES('"
                + values.get(0) + "','"
                + values.get(1) + "','"
                + values.get(2) + "','"
                + values.get(3) + "','"
                + values.get(4) + "','"
                + values.get(5) + "','"
                + values.get(6) + "','"
                + values.get(7) + "','"
                + values.get(8) + "','"
                + values.get(9).replace("\\","\\\\") + "','"
                + values.get(10) + "','"
                + values.get(11) + "')";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void insertIntoTemporary(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "("
                + columns.get(0) + ","
                + columns.get(1) + ","
                + columns.get(2) + ","
                + columns.get(3) + ","
                + columns.get(4) + ","
                + columns.get(5) + ","
                + columns.get(6) + ","
                + columns.get(7) + ","
                + columns.get(8) + ","
                + columns.get(9) + ","
                + columns.get(10) + ","
                + columns.get(11) + ") "
                + "VALUES('"
                + values.get(0) + "','"
                + values.get(1) + "','"
                + values.get(2) + "','"
                + values.get(3) + "','"
                + values.get(4) + "','"
                + values.get(5) + "','"
                + values.get(6) + "','"
                + values.get(7) + "','"
                + values.get(8) + "','"
                + values.get(9).replace("\\","\\\\") + "','"
                + values.get(10) + "','"
                + values.get(11) + "')";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateDatabase(String tableName, String column, String value, String id) {
        String sql = "UPDATE " + tableName
                + " SET " + column + "='" + value + "'"
                + " WHERE Run_ID = '" + id + "';";
        System.out.println(sql);
        try {
            statement.executeUpdate(sql);
            System.out.println("Update database successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void updateDatabase2(String tableName, String column, String value, String suiteName) {
        String sql = "UPDATE " + tableName
                + " SET " + column + "='" + value + "'"
                + " WHERE Test_suite = '" + suiteName + "';";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    

    public void closeConnection(){
        try{
            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
