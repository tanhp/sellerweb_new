package operation;


import com.samczsun.skype4j.formatting.Message;
import com.samczsun.skype4j.formatting.Text;

//import autotestng.AutoTestNG;
import constant.MyConstants;
import database.Database;
import helpers.MyHelper;
import helpers.MyTools;
import logger.MyLogger;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import report.ExtentReport;
import utils.FormatUtils;
import utils.SkypeSender;
import utils.XLSWorker;

import java.awt.*;
import java.lang.reflect.Method;
import java.util.*;
import java.util.List;

public class ParallelTest {

    private String oldFile = null;
    static public WebDriver driver;

    static XSSFWorkbook workbook = null;
    static XSSFSheet sheet = null;
    XSSFWorkbook workbook1 = null;
    static private String fileValidation = null;
    static String sheetName = null;
    static int rowNum = -1;

    ExtentReport report;
    public static Method method;
    public static String className;
    String reportPath = null;
    String reportUrl = null;
    private String id = null;
    private String executionDate = null;
    private String executionTime = null;
    private String executionTimeSuite = null;

    private String testResult = null;
    Database database;
    private List<String> columns = Arrays.asList("Run_ID", "Test_suite", "Test_class", "Test_method", "Execution_time",
            "Execution_time_long", "Duration_time", "First_run", "Second_run", "Final_result", "Report", "Error_screenshot", "Environment");
    private List<String> columns2 = Arrays.asList("Environment","Test_suite",	"Test_method", "Running_status", "Execution_time",
            "Execution_time_long", "Duration_time", "First_run", "Second_run", "Final_result", "Report", "Error_screenshot");
    private List<String> values = new ArrayList<String>();
    private List<String> values2 = new ArrayList<String>();

    private String firstRun = "";
    private String secondRun = "";
    private String finalRun = "";

    private String screenshotPath = "";
    private String screenshotURL = "";
    public static String exception = "";

    private String pSuiteName = null;
    private int count = 0;

    public Boolean flag = true;
    public Boolean flagForHeaderFile = false;
    public Boolean security = false;

    private MyHelper helper;
    UIOperation action;

    public void setHelper(MyHelper helper){
        this.helper = helper;
    }

    public MyHelper getHelper(){
        return this.helper;
    }

    public ParallelTest(){
        report = new ExtentReport();
        database = new Database();
    }

    public void setDriver(WebDriver driver){
        this.driver = driver;
    }

    public WebDriver getDriver(){
        return this.driver;
    }

    public ExtentReport getExtentReport() {
        return report;
    }

    public WebDriver initDriver(String browser){
        switch (browser) {
            case "firefox":
                System.setProperty("webdriver.gecko.driver", MyConstants.FIREFOX_DRIVER_PATH);
                FirefoxOptions ffOptions = new FirefoxOptions();
                ffOptions.addPreference("browser.download.manager.focusWhenStarting", true);
                ffOptions.addPreference("browser.download.useDownloadDir", false);
                ffOptions.addPreference("browser.helperApps.alwaysAsk.force", true);
                ffOptions.addPreference("browser.download.manager.alertOnEXEOpen", false);
                ffOptions.addPreference("browser.download.manager.closeWhenDone", true);
                ffOptions.addPreference("browser.download.manager.showAlertOnComplete", false);
                ffOptions.addPreference("browser.download.manager.useWindow", false);
                ffOptions.addPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                driver = new FirefoxDriver(ffOptions);
                driver.manage().window().maximize();
                break;
            case "chrome":
                try {
                    if(System.getProperty("os.name").startsWith("Linux")){
                        String command = "chmod +x " + MyConstants.getChromeDriverPath();
                        System.out.println(command);
                        Runtime.getRuntime().exec(command);
                    }
                    System.setProperty("webdriver.chrome.driver",MyConstants.getChromeDriverPath());
                    Map<String, Object> prefs = new HashMap<String, Object>();
                    prefs.put("profile.default_content_setting_values.notifications", 2);
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("--disable-extensions");
                    if(System.getProperty("os.name").startsWith("Linux")){
                        options.addArguments("--headless");
                        options.addArguments("window-size=1400,900");
                    }
                    options.addArguments("--start-maximized");
                    options.addArguments("disable-infobars"); // disabling infobars
                    options.addArguments("--disable-extensions"); // disabling extensions
                    options.addArguments("--disable-gpu"); // applicable to windows os only
                    options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
                    options.addArguments("--no-sandbox"); // Bypass OS security model
                    options.setExperimentalOption("prefs", prefs);
                    options.setExperimentalOption("useAutomationExtension", false);
                    driver = new ChromeDriver(options);
                    System.out.println("Driver done!");
                }catch(Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }

                break;
            case "iexplorer":
                System.setProperty("webdriver.ie.driver",MyConstants.IE_DRIVER_PATH);
                driver = new InternetExplorerDriver();
                break;
        }
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    public WebDriver initDriverEmulation(String browser, String device){
        switch (browser) {
            case "firefox":
                System.setProperty("webdriver.gecko.driver", MyConstants.FIREFOX_DRIVER_PATH);
                driver = new FirefoxDriver();
                break;
            case "chrome":
                System.setProperty("webdriver.chrome.driver",MyConstants.getChromeDriverPath());
                Map<String, String> mobileEmulation = new HashMap<String, String>();
                mobileEmulation.put("deviceName", device);
                Map<String, Object> chromeOptions = new HashMap<String, Object>();
                chromeOptions.put("mobileEmulation", mobileEmulation);
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

                driver = new ChromeDriver(capabilities);
                break;
            case "iexplorer":
                System.setProperty("webdriver.ie.driver",MyConstants.IE_DRIVER_PATH);
                driver = new InternetExplorerDriver();
                break;
        }
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    public static FirefoxProfile FirefoxDriverProfile() throws Exception {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", "downloadPath"); // pass string downloadpath
        profile.setPreference("browser.helperApps.neverAsk.openFile",
                "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
                "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
        profile.setPreference("browser.download.manager.focusWhenStarting", false);
        profile.setPreference("browser.download.manager.useWindow", false);
        profile.setPreference("browser.download.manager.showAlertOnComplete", false);
        profile.setPreference("browser.download.manager.closeWhenDone", false);
        return profile;
    }

    public void closeDriver(String browser){
        switch (browser){
            case "firefox":
                driver.quit();
                break;
            case "chrome":
                driver.close();
                break;
        }
    }

    public void killProcess(String browser) {
        try {
            if (browser.equals("chrome")) {
                Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
                Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
            } else if (browser.equals("firefox")) {
                Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
                Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
            } else if (browser.equals("iexplorer")) {

            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public void performTestCaseUsingExcel(String str_sheetName, int testCaseId, String fileProperties){
        String fileAction = "TestAction.xlsx";
        String str_fileValidation = "TestValidation.xlsx";
        System.out.println(getHelper());
        action = new UIOperation(getHelper());
        XLSWorker.setSheetName(str_sheetName);
        workbook = XLSWorker.getWorkbook(MyConstants.INPUT_PATH + str_fileValidation);
        sheet = workbook.getSheet(str_sheetName);
        fileValidation = str_fileValidation;
        sheetName = str_sheetName;
        rowNum = XLSWorker.getRowNumByTestCaseID(sheet, String.valueOf(testCaseId));

        Object[][] objects = XLSWorker.getDataForTestCase(fileAction, fileValidation, sheetName,rowNum);
        if(objects==null)
            return;
        for(int i=0; i< objects.length; i++){
            String keyword = objects[i][1].toString();
            String object = objects[i][2].toString();
            String value = objects[i][3].toString();
            MyLogger.info(keyword + " - " + object + " - " + value);
            action.perform(fileProperties,keyword,object,value);
        }
        //XLSWorker.updateExcel(XLSWorker.getSheet(workbook,sheetName),rowNum,"PASSED");
    }


    public void closeExcel(){
        XLSWorker.writeExcel(workbook);
    }

    public void setUpReport(ITestContext iSuite){
        Date date = new Date();
        String suiteName = iSuite.getCurrentXmlTest().getSuite().getName();
        pSuiteName = suiteName;
        if(suiteName.equals("Chrome") || suiteName.equals("Firefox") || suiteName.equals("UAT")) {
            security = true;
        }
        String browser = StringUtils.capitalize(iSuite.getCurrentXmlTest().getParameter("browser"));
        reportPath = MyConstants.HTMLREPORT_PATH + suiteName + " - " + browser + " - "
                + FormatUtils.formatDate(date, "dd-MM-yyyy HH.mm.ss") + ".html";
        reportUrl = MyConstants.HTMLREPORT_URL + suiteName + " - " + browser + " - "
                + FormatUtils.formatDate(date, "dd-MM-yyyy HH.mm.ss") + ".html";
        report.initExtent(reportPath);

    }

    public void setUpDatabase(){
        database.getConnection();
        database.useDatabase(MyConstants.DB_DATABASENAME);
    }


    public void setUpTestCase(Method methodName){
        report.startExtent(methodName.getName());
        method = methodName;
    }

    public void setUpTestClass(String className1) {
        className = className1;
    }

    public void updateExcelFinalResult(){
        System.out.println("Update excel final result for " + sheetName);
        if(testResult.equals("FAILED")) {
            testResult = "FAILED: " + exception;
        }
        if(security == true) {
            this.workbook1 = XLSWorker.getWorkbook(MyConstants.EXCEL_OUTPUT_PATH + "result_TestValidation_" + sheetName + ".xlsx");
            if(this.workbook1 == null) {
                this.workbook1 = XLSWorker.getWorkbook(MyConstants.INPUT_PATH + fileValidation);
            }
            XLSWorker.updateExcel(XLSWorker.getSheet(workbook1,sheetName),rowNum,testResult,screenshotURL);
            XLSWorker.writeExcel(workbook1);
        }else {
            XLSWorker.updateExcel(XLSWorker.getSheet(workbook,sheetName),rowNum,testResult,screenshotURL);
            System.out.println("A");
            XLSWorker.writeExcel(workbook);
            System.out.println("B");
        }
    }



    public void updateTestResult(ITestResult iResult){
        try {
            System.out.println("Update test result...");
            if (iResult.getStatus() == ITestResult.FAILURE || iResult.getStatus() == ITestResult.SKIP) {
                testResult = "FAILED";
                MyLogger.info("The testcase is failed");
                screenshotPath = getHelper().myTools().takeScreenshot(method.getName());
                System.out.println("Screenshot: " + screenshotPath);
                screenshotURL = getHelper().myTools().getImageUrl();
            } else if (iResult.getStatus() == ITestResult.SUCCESS) {
                testResult = "PASSED";
            }
            MyLogger.info("Test result is: " + testResult);
        }catch(Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void insertIntoReport(){
        //report.updateExtent(testResult, method.getName(), exception, screenshotPath);
        System.out.println("Insert result into report...");
        report.updateExtent(testResult, method.getName(), exception, screenshotURL);
        report.endExtent();
    }

    public void insertIntoDatabase(ITestResult iResult, ITestContext iContext){
        long duration = iResult.getEndMillis() - iResult.getStartMillis();
        long second = (duration / 1000) % 60;
        long minute = (duration / (1000 * 60)) % 60;
        long hour = (duration / (1000 * 60 * 60)) % 24;
        String durationStr = String.format("%02d:%02d:%02d", hour, minute, second, duration);
        String suiteName = iContext.getCurrentXmlTest().getSuite().getName();

        executionDate = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset,
                "yyyy-MM-dd");
        executionTime = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset,
                "yyyy-MM-dd HH:mm:ss");
        id = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset, "ddMMyyHHmmssSSS");

        if(firstRun.equals("")) {
            firstRun = testResult;
        }

        if(firstRun.equals("PASSED")){
            secondRun = "PASSED";
            finalRun = secondRun;
            count = 2;
        }else{
            count++;
            if(count==2) {
                secondRun = testResult;
                finalRun = secondRun;
                if(finalRun.equals("PASSED")) {
                    screenshotURL = "";
                }
            }
        }

        if(count==2) {
            values.add(0, id);
            values.add(1, suiteName);
            values.add(2, className);
            values.add(3, method.getName());
            values.add(4, executionDate);
            values.add(5, executionTime);
            values.add(6, durationStr);
            values.add(7, firstRun);
            values.add(8, secondRun);
            values.add(9, finalRun);
            values.add(10, reportUrl);
            values.add(11, screenshotURL);
            values.add(12, MyConstants.ENVIRONMENT);
            MyLogger.info("Run_ID: " + id);
            MyLogger.info("TestSuite: " + iContext.getCurrentXmlTest().getSuite().getName());
            MyLogger.info("TestMethod: " + method.getName());
            MyLogger.info("Execution Date: " + executionDate);
            MyLogger.info("Execution Time: " + executionTime);
            MyLogger.info("Duration: " + durationStr);
            MyLogger.info("First Run: " + firstRun);
            MyLogger.info("Second Run: " + secondRun);
            MyLogger.info("Final Run: " + finalRun);
            MyLogger.info("Report URL: " + reportUrl);
            MyLogger.info("Screenshot URL: " + screenshotURL);
            MyLogger.info("Environment: " + MyConstants.ENVIRONMENT);

            MyLogger.info("Insert result into database...");
            //database.insertIntoDatabase(MyConstants.DB_TABLENAME, columns, values);
            updateExcelFinalResult();
            sendFailedResultToSkype(suiteName);
            count = 0;
            firstRun = "";
            secondRun = "";
            finalRun = "";
            screenshotURL = "";
        }

    }

    public static int getRandom(){
        Random rand = new Random();
        int a =  rand.nextInt(100);
        System.out.println("Para a: " + a);
        return a;
    }

    public void closeReport(){
        report.closeExtent();
    }

    public void closeDatabase(){
        MyLogger.info("Closing database...");
        database.closeConnection();
    }

    public void insertIntoHeaderFile(ITestContext testSuite){
        MyLogger.info("Updating results in header file...");
        int noOfPass = testSuite.getPassedTests().size();
        int noOfFail = testSuite.getFailedTests().size();

        XSSFWorkbook workbook = XLSWorker.getWorkbook(MyConstants.EXCEL_OUTPUT_PATH + "0Header.xlsx");
        XSSFSheet sheet = XLSWorker.getSheet(workbook,"Header");
        String testSuiteName = testSuite.getCurrentXmlTest().getSuite().getName();

        int row = -1;
        switch (testSuiteName){
            case "Login":
                row = 1;
                break;
            case "Chat":
                row = 2;
                break;
            case "AddNewProduct":
                row = 3;
                break;
            case "EditProduct":
                row = 4;
                break;
            case "Instock":
                row = 5;
                break;
            case "QuickEdit":
                row = 6;
                break;
            case "Promotion":
                row = 7;
                break;
            case "ShippingSupport":
                row = 8;
                break;
            case "PrivateOffer":
                row = 9;
                break;
            case "ShopInfo":
                row = 10;
                break;
            case "CancelOrder":
                row = 11;
                break;
            case "CarrierConfig":
                row = 12;
                break;
            case "Voucher":
                row = 13;
                break;
            case "Chrome":
                row = 14;
                break;
            case "Firefox":
                row = 15;
                break;
            case "UAT":
                row = 16;
                break;
        }
        System.out.println("Row: " + row);
        XLSWorker.updateHeaderResult(sheet,row,noOfPass,noOfFail);
        XLSWorker.writeHeaderFile(workbook);
    }

    public void insertIntoJSONFile(ITestContext testSuite){
        MyLogger.info("Write test result into Json file...");
        executionTimeSuite = FormatUtils.getCurrentTimeByTimezoneOffset(
                MyConstants.timezoneOffset, "yyyy-MM-dd HH:mm:ss");
        JSONObject result = new JSONObject();
        result.put("Passed", testSuite.getPassedTests().size());
        result.put("Failed", testSuite.getFailedTests().size());
        // result.put("Pending", testSuite.getSkippedTests().size());
        result.put("ExecutionTime", executionTimeSuite.substring(5, executionTimeSuite.length()));

        JSONArray resultList = new JSONArray();
        resultList.put(result);

        JSONObject suite = new JSONObject();
        suite.put(testSuite.getCurrentXmlTest().getSuite().getName(), resultList);
        getHelper().myTools().writeJSONFile(suite, "resultSeller.json");
    }

    public void sendFailedResultToSkype(String suiteName){
        if(true){
            MyLogger.info("Send failed result via Skype...");
            Message skypeMessage = Message.create()
                    .with(Text.rich("====SELLER - Automation - "))
                    .with(Text.rich("\n TestSuite: " + suiteName).withBold())
                    .with(Text.rich(" - "))
                    .with(Text.rich("WARNING REPORT").withColor(Color.RED).withBold())
                    .with(Text.rich("\n Date: " + executionTime))
                    .with(Text.rich("\n TestCase: " + method.getName()).withBold())
                    .with(Text.rich("\n Result: " + testResult))
                    .with(Text.rich("\n Screenshot: " + screenshotURL));
            if(suiteName.equals("UAT")) {
                SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_TAN, skypeMessage, 1);
            }else {
                SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_TAN, skypeMessage, 1);
            }
        }
    }

    public void insertIntoTemporary(ITestContext iContext){
        //values2.add(0, id);
        values2.add(0, "SELLER-PRO");
        values2.add(1, iContext.getCurrentXmlTest().getSuite().getName());
        values2.add(2, "");
        values2.add(3, "Running");
        values2.add(4, "0000-00-00");
        values2.add(5, "0000-00-00 00:00:00");
        values2.add(6, "00:00:00");
        values2.add(7, "");
        values2.add(8, "");
        values2.add(9, "");
        values2.add(10, "");
        values2.add(11, "");

        database.insertIntoTemporary("temporary_seller", columns2, values2);
    }

    public void updateStatusWithSuiteName(ITestContext iContext, String status) {
        String suiteName = iContext.getCurrentXmlTest().getSuite().getName();
        database.updateDatabase2("temporary_seller", "Running_status", status, suiteName);
    }

    public void updateResultWithSuiteName(ITestContext iContext) {
        String suiteName = iContext.getCurrentXmlTest().getSuite().getName();
        database.updateDatabase2("temporary_seller", "Final_result", testResult, suiteName);
    }


}
