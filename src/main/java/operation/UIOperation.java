package operation;

import helpers.*;
import org.openqa.selenium.By;

import pages.authentication.Authentication;
import utils.ConfigReader;

public class UIOperation{

    private MyHelper helper;

    public UIOperation(MyHelper helper){
        this.helper = helper;
    }

    public void perform(String fileProperties, String keyword, String object, String value){
        By selector = null;
        if(!object.equals("")){
            if(!object.contains("ecom_shipping") && !object.contains("BlogContent"))
                selector = getObject(fileProperties,object);
        }
        switch(keyword) {
            case "GOTOURL":
                helper.myNavigagtes().navigateToURL(value);
                break;
            case "SELECT":
                helper.myBasics().click(selector);
                break;
            case "SETTEXT":
                if (!value.equals("NO")) {
                    helper.myBasics().sendKeys(selector, value);
                }
                break;
            case "SUBMIT":
                if(value.equals("YES")) {
                    helper.myJavascripts().scrollToThenClick(selector);
                }
                break;
            case "VALIDATION":
                helper.myVerifys().validation(fileProperties, value);
                break;
            case "LOGIN":
                if(value.equals("YES")){
                    Authentication.loginAs(value);
                }
        }
    }

    public static By getObject(String fileName, String object)
    {
        String values = ConfigReader.fileRead(fileName,object);
        String[] spl = values.split(";",2);

        //find by xpath
        if(spl[1].equalsIgnoreCase("XPATH"))
        {
            return By.xpath(spl[0]);
        }

        //find by class
        else if(spl[1].equalsIgnoreCase("CLASSNAME"))
        {
            System.out.println("CLASSNAME: " + object);
            return By.className(spl[0]);
        }

        //find by id
        else if(spl[1].equalsIgnoreCase("ID"))
        {
            return By.id(spl[0]);
        }

        //find by name
        else if(spl[1].equalsIgnoreCase("NAME"))
        {
            return By.name(spl[0]);
        }

        //find by css
        else if(spl[1].equalsIgnoreCase("CSS"))
        {
            return By.cssSelector(spl[0]);
        }

        //find by link
        else if(spl[1].equalsIgnoreCase("LINKTEXT"))
        {
            return By.linkText(spl[0]);
        }

        //find by partial link
        else if(spl[1].equalsIgnoreCase("PARTIALLINK"))
        {
            return By.partialLinkText(spl[0]);
        }
        else
        {
            return null;
        }
    }
}
