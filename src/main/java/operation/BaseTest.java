package operation;

import constant.MyConstants;

import helpers.MyHelper;
import logger.MyLogger;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class BaseTest extends ParallelTest{
    @BeforeSuite
    public void suiteSetUp(ITestContext iSuite){
    	String testSuiteName = iSuite.getCurrentXmlTest().getSuite().getName();
        MyLogger.info("Start test suite: " + testSuiteName);
        setUpReport(iSuite);
        //setUpDatabase();
        //updateStatusWithSuiteName(iSuite,"Running"); // website
    }


    @BeforeClass
    public void classSetUp(){

    }

    @Parameters("browser")
    @BeforeMethod(alwaysRun = true)
    public void testCaseSetUp(Method methodName, @Optional(MyConstants.BROWSER) String browser){
        MyLogger.info("Running testcase: " + methodName.getName());
        setDriver(initDriver(browser));
        //setDriver(initDriverEmulation(browser,"iPhone 6"));
        setHelper(new MyHelper(getDriver()));
        setUpTestCase(methodName);
    }

    @Parameters("browser")
    @AfterMethod
    public void testCaseTearDown(ITestResult iResult, ITestContext iContext, @Optional(MyConstants.BROWSER) String browser){
        updateTestResult(iResult);
        insertIntoReport();
        insertIntoDatabase(iResult,iContext);
        //sendToSlack();
        //sendToEmail();
        closeDriver(browser);
    }

    @AfterClass
    public void classTearDown(){

    }

    @Parameters("browser")
    @AfterSuite
    public void suiteTearDown(ITestContext testSuite, @Optional(MyConstants.BROWSER) String browser){
        if(flag)
            //closeExcel();
        closeReport();
        //closeDatabase();
        //insertIntoHeaderFile(testSuite);
        updateExcelFinalResult();
        if(!System.getProperty("os.name").startsWith("Linux")) {
        	insertIntoJSONFile(testSuite);
            if(!System.getProperty("os.name").startsWith("Windows 10"))
        	    killProcess(browser);
        }
    }
}
