package utils;

import constant.MyConstants;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {
    public static String fileRead(String fileName, String objKey) {
        FileInputStream fis;
        try {
            fis = new FileInputStream(MyConstants.OBJECT_PATH + fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Cant't read config.properties file!");
            return "";
        }
        Properties p = new Properties();
        try {
            p.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Cant't read config.properties file!");
            return "";
        }
        String objValue = p.getProperty(objKey);
        return objValue;
    }
}
