package utils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.util.List;
import java.util.Properties;

public class EmailSender {
    private EmailSender(){}

    public static void sendFromGMail(String from, String pass, String[] to, String subject, String body){
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];
            for( int i = 0; i < to.length; i++ ) {
                toAddress[i] = new InternetAddress(to[i]);
            }
            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (Exception e) {
            System.out.println((e.getMessage()));
        }
    }
    
    public static void sendNotificationMail(String subject, String distrolist, String mailMessage, List<String> files) {
        String username = "tester01gm@gmail.com";
        String password = "12345678Ac";

        Properties props = new Properties();
        //props.put("proxySet", true);
        //props.setProperty("socksProxyHost","111.65.244.252");
        //props.setProperty("socksProxyPort","80");
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        String[] recipients = distrolist.split(",");
        InternetAddress[] address = new InternetAddress[recipients.length];
        for (int i = 0; i < recipients.length; i++) {
            try {
                address[i] = new InternetAddress(recipients[i]);
            } catch (AddressException e) {
                e.printStackTrace();
            }
        }

        try {
            BodyPart messageBodyPart = new MimeBodyPart();
            Multipart multipart = new MimeMultipart();
            messageBodyPart.setContent(mailMessage, "text/html; charset=utf-8");
            multipart.addBodyPart(messageBodyPart);
            if(files != null) {
                for(int i = 0; i < files.size(); i++) {
                    messageBodyPart = new MimeBodyPart();
                    DataSource source = new FileDataSource(files.get(i));
                    messageBodyPart.setDataHandler(new DataHandler(source));
                    messageBodyPart.setFileName(files.get(i));
                    multipart.addBodyPart(messageBodyPart);
                }
            }

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, address);
            message.setSubject(subject);
            //message.setContent(mailMessage, "text/html; charset=utf-8");
            message.setContent(multipart);
            System.out.println("Sending mail...");
            Transport.send(message);
            System.out.println("Sent successfully!!!");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
