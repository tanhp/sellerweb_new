package utils;


import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackMessage;

public class SlackSender {

    public static void pushNotifyToSlack(String channel, String username, String text){
        // Send simple message in different room with custom name
        SlackApi api = new SlackApi("https://hooks.slack.com/services/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");
        //api.call(new SlackMessage("#thong-bao", "automation-tester", "I'll send a message to slack from java!"));
        api.call(new SlackMessage(channel, username, "Hi @tanhp : " + text).setLinkNames(true));
    }
}
