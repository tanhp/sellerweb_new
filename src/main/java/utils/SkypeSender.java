package utils;

import com.samczsun.skype4j.Skype;
import com.samczsun.skype4j.SkypeBuilder;
import com.samczsun.skype4j.chat.Chat;
import com.samczsun.skype4j.exceptions.ChatNotFoundException;
import com.samczsun.skype4j.exceptions.ConnectionException;
import com.samczsun.skype4j.exceptions.InvalidCredentialsException;
import com.samczsun.skype4j.exceptions.NotParticipatingException;
import com.samczsun.skype4j.formatting.Message;

import java.util.List;

public class SkypeSender {
    public static void sendSkypeMessage(String username, String password, String identity, Message message, int messageAmount) {
        Skype skype = new SkypeBuilder(username, password).withAllResources().build();

        try {
            skype.login();

            Chat chat = skype.loadChat(identity);
            for(int i = 1; i <= messageAmount; i++) {
                chat.sendMessage(message);
            }
            System.out.println("Message sent!");

            skype.logout();
        } catch (InvalidCredentialsException e) {
            e.printStackTrace();
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (NotParticipatingException e) {
            e.printStackTrace();
        } catch (ChatNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    public static void getIdentities(String username, String password) {
        Skype skype = new SkypeBuilder(username, password).withAllResources().build();
        List<Chat> chatList = null;
        try {
            skype.login();
            chatList = skype.loadMoreChats(100);
            skype.logout();
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (NotParticipatingException e) {
            e.printStackTrace();
        } catch (InvalidCredentialsException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < chatList.size(); i++) {
            System.out.println(chatList.get(i).getIdentity());
        }
    }
}
