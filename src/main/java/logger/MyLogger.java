package logger;

import org.apache.log4j.*;

public class MyLogger{

    private static Logger logger = Logger.getLogger(MyLogger.class.getName());

    private MyLogger(){
    }

    public static void info(String message){
        logger.info(message);
    }

    public static void warn(String message){
        logger.warn(message);
    }

    public static void error(String message){
        logger.error(message);
    }

    public static void fatal(String message){
        logger.fatal(message);
    }

    public static void debug(String message){
        logger.debug(message);
    }

    public static void debug(Object message, Throwable t){
        logger.debug(message,t);
    }
}