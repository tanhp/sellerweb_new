import operation.BaseTest;
import org.testng.annotations.Test;
import utils.SkypeSender;
import utils.SlackSender;

public class TC_Login extends BaseTest {
    @Test
    public void C001_LoginWithNoUsername(){
        performTestCaseUsingExcel("Login",11, "Login.properties");
    }

    @Test
    public void C002_LoginWithNoPassword(){
        performTestCaseUsingExcel("Login",22, "Login.properties");
    }

    //@Test
    public void C003_LoginWithInvalidData(){
        performTestCaseUsingExcel("Login",4, "Login.properties");
    }

    //@Test
    public void C004_LoginWithValidData(){
        performTestCaseUsingExcel("Login",5, "Login.properties");
    }

    //@Test
    public void C005_SQLInjection(){
        performTestCaseUsingExcel("Login",6, "Login.properties");
    }

    //@Test
    public void C006_LoginWithEmail_NormalShop(){
        performTestCaseUsingExcel("Login",7, "Login.properties");
    }

    //@Test
    public void C007_LoginWithPhoneNumber_NormalShop(){
        performTestCaseUsingExcel("Login",8, "Login.properties");
    }

    //@Test
    public void C008_LoginWithEmail_SenmallShop(){
        performTestCaseUsingExcel("Login",9, "Login.properties");
    }

    //@Test
    public void C009_LoginWithPhoneNumber_SenmallShop(){
        performTestCaseUsingExcel("Login",10, "Login.properties");
    }

    //@Test
    public void C010_LoginWithAccountNoExists(){
        performTestCaseUsingExcel("Login",11, "Login.properties");
    }

}
